import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import database untuk Query Builder
// import Database from '@ioc:Adonis/Lucid/Database'
import CreateFieldValidator from 'App/Validators/CreateFieldValidator'
// import Model untuk ORM
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
    public async index({request, response}: HttpContextContract) {
        if (request.qs().name) {
            // Query Builder
            // let name = request.qs().name
            // let filteredField = await Database.from('fields').select('id', 'name', 'address', 'phone').where('name', name)
            
            // Model ORM
            let name = request.qs().name
            let filteredField = await Field.findBy('name', name)
            return response.ok({status: 'success', message: "success get fields", data: filteredField})
            

        } else {
            // Query Builder
            // let fields = await Database.from('fields').select('id', 'name', 'address', 'phone')
            
            // Model ORM
            let fields = await Field.all()
            return response.ok({status: 'success', message: "success get fields", data: fields})
            }
        }

    public async store({request, response, params}: HttpContextContract) {
        try {
            await request.validate(CreateFieldValidator)
            //Query Builder
            // let newUserId = await Database.table('fields').returning('id').insert({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     phone: request.input('phone')
            // })

            //Model ORM
            const venue = await Venue.findByOrFail('id', params.venue_id)
            const newField = new Field()
            newField.name = request.input('name'),
            newField.type = request.input('type')
            await newField.related('venue').associate(venue)
            
            return response.created({message: "berhasil menambahkan data field baru", data: newField})

        } catch (error) {
            response.badRequest(error.messages)
        }
    }

    public async show({response, params}: HttpContextContract) {
        // Query Builder
        // let fields = await Database.from('fields').select('id', 'name', 'address', 'phone').where('id', params.id).firstOrFail()
        
        // Model ORM
        let fields = await Field.query().where('id', params.id).preload('bookings', (bookingQuery) => {
            bookingQuery.select(['title', 'play_date_start', 'play_date_end', 'user_id'])
        }).firstOrFail()

        response.ok({status: 'success', message: "succes get fields", data: fields})
    }

    public async update({request, response, params}: HttpContextContract) {
        // Query Builder
        // await Database.from('fields').where('id', params.id).update({
        //     name: request.input('name'),
        //     address: request.input('address'),
        //     phone: request.input('phone')
        // })

        // Model ORM
        let field = await Field.findOrFail(params.id)
        field.name = request.input('name')
        field.type = request.input('type')
        field.venue = request.input('venue_id')
        field.save()

        return response.ok({status: 'success', message: 'field data updated', data: field})
    }
    
    public async destroy({response, params}: HttpContextContract) {
        // Query Builder
        // await Database.from('fields').where('id', params.id).delete()

        // Model ORM
        let field = await Field.findOrFail(params.id)
        await field.delete()
        
        return response.ok({status: 'success', message: 'field data deleted'})        
    }
}
