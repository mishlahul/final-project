import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import CreateBookingValidator from 'App/Validators/CreateBookingValidator'

export default class BookingsController {
    /**
     * 
     * @swagger 
     * /fields/:field_id/bookings:
     *  post:
     *      tags: 
     *          -   Project
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www/form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/User'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/User'
     *      responses:
     *          200:
     *              description: booking created, send success message
     *              example:
     *                  message: Success
     *          400:
     *              description: booking not created, send bad request message
     *              example:
     *                  message: Error, invalid input
     */
    public async store({request, response, params, auth}: HttpContextContract) {
        const field = await Field.findByOrFail('id', params.field_id)
        const user = auth.user!
        
        try {
            const payload = await request.validate(CreateBookingValidator)
            const booking = new Booking
            booking.playDateStart = payload.play_date_start
            booking.playDateEnd = payload.play_date_end
            booking.title = payload.title

            booking.related('field').associate(field)
            user.related('myBookings').save(booking)
            return response.created({status: 'success', data: booking})

        } catch (error) {
            response.badRequest(error.messages)
        }
    }
    /**
     * 
     * @swagger 
     * /fields/{:field_id}/bookings/{:id}:
     *  get:
     *      tags: 
     *          -   Project
     *      parameters:
     *          - in: path
     *              name: field_id
     *              required: true
     *              schema:
     *                  type: integer
     *              style: simple
     *              explode: false
     *          - in: path
     *              name: id
     *              required: true
     *              schema:
     *                  type: integer
     *              style: simple
     *              explode: false
     *      responses:
     *          200:
     *              description: booking created, send success message
     *              example:
     *                  message: Success
     *          400:
     *              description: booking not created, send bad request message
     *              example:
     *                  message: Error, invalid input
     */
    public async show({response, params}:HttpContextContract){
        const booking = await Booking.query().where('id', params.id).preload('players', (userQuery) => {
            userQuery.select(['name', 'email', 'id'])
        }).withCount('players').firstOrFail()

        return response.ok({status: 'success', message: 'success get bookings', data: booking})
    }
    /**
     * 
     * @swagger 
     * /bookings/:id:
     *  put:
     *      tags: 
     *          -   Project
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www/form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/User'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/User'
     *      responses:
     *          200:
     *              description: booking created, send success message
     *              example:
     *                  message: Success
     *          400:
     *              description: booking not created, send bad request message
     *              example:
     *                  message: Error, invalid input
     */
    public async join({response, auth, params}: HttpContextContract) {
        const booking = await Booking.findOrFail(params.id)
        let user = auth.user!
        const chekJoin = await Database.from('schedules').where('booking_id', params.id).where('user_id', user.id).first()
        if(!chekJoin) {
            await booking.related('players').attach([user.id])
        } else {
            await booking.related('players').detach([user.id])
        }

        return response.ok({status: 'success', message: 'successfully join/unjoin game'})
    }
}