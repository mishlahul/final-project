import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
// import database untuk Query Builder
// import Database from '@ioc:Adonis/Lucid/Database'
import CreateVenueValidator from 'App/Validators/CreateVenueValidator'
// import Model untuk ORM
import Venue from 'App/Models/Venue'

export default class VenuesController {
    public async index({request, response}: HttpContextContract) {
        if (request.qs().name) {
            // Query Builder
            // let name = request.qs().name
            // let filteredVenue = await Database.from('venues').select('id', 'name', 'address', 'phone').where('name', name)
            
            // Model ORM
            let name = request.qs().name
            let filteredVenue = await Venue.findBy('name', name)
            return response.ok({status: 'success', message: "succes get venues", data: filteredVenue})
            

        } else {
            // Query Builder
            // let venues = await Database.from('venues').select('id', 'name', 'address', 'phone')
            
            // Model ORM
            let venues = await Venue.all()
            return response.ok({status: 'success', message: "succes get venues", data: venues})
            }
        }

    public async store({request, response}: HttpContextContract) {
        try {
            await request.validate(CreateVenueValidator)
            //Query Builder
            // let newUserId = await Database.table('venues').returning('id').insert({
            //     name: request.input('name'),
            //     address: request.input('address'),
            //     phone: request.input('phone')
            // })

            //Model ORM
            let newVenue = new Venue
            newVenue.name = request.input('name'),
            newVenue.address = request.input('address'),
            newVenue.phone = request.input('phone')

            await newVenue.save()
            console.log(newVenue.$isPersisted)
            return response.created({message: "created!", data: newVenue})

        } catch (error) {
            response.badRequest(error.messages)
        }
    }

    public async show({response, params}: HttpContextContract) {
        // Query Builder
        // let venues = await Database.from('venues').select('id', 'name', 'address', 'phone').where('id', params.id).firstOrFail()
        
        // Model ORM
        let venues = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        return response.ok({status: "success", message: 'berhasil get data venue by id', data: venues})
    }

    public async update({request, response, params}: HttpContextContract) {
        // Query Builder
        // await Database.from('venues').where('id', params.id).update({
        //     name: request.input('name'),
        //     address: request.input('address'),
        //     phone: request.input('phone')
        // })

        // Model ORM
        let venue = await Venue.findOrFail(params.id)
        venue.name = request.input('name')
        venue.address = request.input('address')
        venue.phone = request.input('phone')
        venue.save()

        return response.ok({status: 'success', data: venue})
    }
    
    public async destroy({response, params}: HttpContextContract) {
        // Query Builder
        // await Database.from('venues').where('id', params.id).delete()

        // Model ORM
        let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        
        return response.ok({message: 'venue data deleted'})        
    }
}
